#!/usr/bin/env python3

import argparse
import json
import logging
import os
import sys

import yandexcloud
from yandex.cloud.compute.v1.instance_service_pb2 import ListInstancesRequest
from yandex.cloud.compute.v1.instance_service_pb2_grpc import \
    InstanceServiceStub

logger = logging.getLogger(__name__)


class AnsibleInventory:
    def __init__(self):
        self.__groups = {
            'all': {
                'children': [
                    'ungrouped'
                ]
            },
            'ungrouped': {
                'hosts': [],
                'children': []
            },
            '_meta': {
                'hostvars': {}
            }
        }
        self.__hosts = dict()
    
    def __str__(self):
        return json.dumps(self.__groups)

    def get_host_info(self, host_name):
        return self.__hosts.get(host_name, None)

    def add_host_to_inventory(self, host):
        host_name = host.fqdn
        group_name = host.labels.get('ansible_group', None)
        self.__add_host_to_group(host_name, group_name)
        self.__hosts[host_name] = {
            'ansible_host': host.network_interfaces[0].primary_v4_address.one_to_one_nat.address
        }
        for key, value in self.__hosts[host_name].items():
            self.__add_meta_to_host(host_name, key, value)

    def __add_host_to_group(self, host_name, group_name):
        if group_name is None:
            self.__groups['ungrouped']['hosts'].append(host_name)
        else:
            self.__add_group(group_name)
            self.__groups[group_name]['hosts'].append(host_name)

    def __add_group(self, group_name):
        if group_name not in self.__groups:
            self.__groups[group_name] = {
                'hosts': []
            }
            self.__groups['all']['children'].append(group_name)

    def __add_meta_to_host(self, host_name, key, value):
        if host_name not in self.__groups['_meta']['hostvars']:
            self.__groups['_meta']['hostvars'][host_name] = {}
        self.__groups['_meta']['hostvars'][host_name][key] = value


def parse_args():
    parser = argparse.ArgumentParser()
    host_or_list = parser.add_mutually_exclusive_group(required=True)
    host_or_list.add_argument(
        '--list',
        action='store_true',
        help='Output all hosts info, works as inventory script',
    )
    host_or_list.add_argument(
        '--host',
        type=str,
        help='Output specific host info, works as inventory script',
    )
    return parser.parse_args()


def main():
    iam_token = os.getenv('TF_VAR_yc_iam_token', None)
    folder_id = os.getenv('TF_VAR_yc_folder_id', None)
    if iam_token is None:
        logger.error('No iam token was provided')
        sys.exit(1)
    if folder_id is None:
        logger.error('No folder id was provided')
        sys.exit(1)
    args = parse_args()
    yandex_sdk = yandexcloud.SDK(iam_token=iam_token)
    list_vms = yandex_sdk.client(InstanceServiceStub)
    hosts = list_vms.List(ListInstancesRequest(folder_id=folder_id))
    inventory = AnsibleInventory()
    for host in hosts.instances:
        inventory.add_host_to_inventory(host)
    if args.list:
        print(inventory)
    else:
        print(json.dumps(inventory.get_host_info(args.host)))


if __name__ == '__main__':
    main()
