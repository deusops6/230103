# 230103

## Terraform
[terraform](terraform)

Пример terraform.tfvars

```
# yc iam create-token
# yc_iam_token = ""
# yc config get cloud-id
# yc_cloud_id = ""
# yc config get folder-id
# yc_folder_id = ""
yc_zone = "ru-central1-a"

net_230103_name = "net_230123"

vms_subneta_name           = "vmsubnet-a"
vms_subneta_zone           = "ru-central1-a"
vms_subneta_v4_cidr_blocks = ["192.168.10.0/24"]

swarm_image_family         = "ubuntu-2204-lts"
swarm_leader_host_count    = 2
swarm_leader_platform_id   = "standard-v2"
swarm_leader_preemptible   = true
swarm_leader_cores         = 2
swarm_leader_memory        = 2
swarm_leader_core_fraction = 20
swarm_leader_disk_size     = 10
swarm_leader_ssh_key_user  = "ubuntu"
swarm_leader_ssh_key_path  = "~/.ssh/id_rsa.pub"
swarm_leader_labels = {
  "ansible_group" = "swarm_leader"
}

swarm_worker_host_count    = 4
swarm_worker_platform_id   = "standard-v2"
swarm_worker_preemptible   = true
swarm_worker_cores         = 2
swarm_worker_memory        = 2
swarm_worker_core_fraction = 20
swarm_worker_disk_size     = 10
swarm_worker_ssh_key_user  = "ubuntu"
swarm_worker_ssh_key_path  = "~/.ssh/id_rsa.pub"
swarm_worker_labels = {
  "ansible_group" = "swarm_worker"
}

```

```
cd terraform
source ./env
terraform init -backend-config=dev.tfbackend
terraform apply --auto-approve
```

## Установка python зависимостей для dynamic inventory
```
python3 -m venv venv
source venv/bin/activate
python3 -m pip install -r ansible/requirements.txt
```

## Ansible
[ansible](ansible)
 - Заполнить minio_access_key и minio_secret_key в [yc_cloud/group_vars/swarm_leader.yml](ansible/yc_cloud/group_vars/swarm_leader.yml)
 - Установить роли через galaxy
 - Выполнить playbook [minio.yml](ansible/minio.yml)

```
cd ansible
ansible-galaxy install -r requirements.yml
ansible-playbook minio.yml
```
