#!/bin/bash

set -ex

python3 -m venv venv
source venv/bin/activate
python3 -m pip install -r ansible/requirements.txt

cd terraform
source ./env
terraform init -backend-config=dev.tfbackend
terraform apply --auto-approve

# wait for VMs to startup
sleep 60


cd ../ansible
ansible-galaxy install -r requirements.yml
ansible-playbook minio.yml
