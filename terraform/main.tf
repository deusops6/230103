terraform {
  # use -backend-config=dev.tfbackend
  backend "s3" {
  }

  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.82.0"
    }
  }
}

provider "yandex" {
  token     = var.yc_iam_token
  cloud_id  = var.yc_cloud_id
  folder_id = var.yc_folder_id
  zone      = var.yc_zone
}
