resource "yandex_vpc_network" "net_230103" {
  name = var.net_230103_name
}

resource "yandex_vpc_subnet" "vm_subneta" {
  name           = var.vms_subneta_name
  zone           = var.vms_subneta_zone
  network_id     = yandex_vpc_network.net_230103.id
  v4_cidr_blocks = var.vms_subneta_v4_cidr_blocks
}

data "yandex_compute_image" "swarm_os_image" {
  family = var.swarm_image_family
}

resource "yandex_compute_instance" "swarm_leaders" {
  count       = var.swarm_leader_host_count
  folder_id   = var.yc_folder_id
  platform_id = var.swarm_leader_platform_id
  resources {
    cores         = var.swarm_leader_cores
    memory        = var.swarm_leader_memory
    core_fraction = var.swarm_leader_core_fraction
  }
  scheduling_policy {
    preemptible = var.swarm_leader_preemptible
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.swarm_os_image.id
      size     = var.swarm_leader_disk_size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm_subneta.id
    nat       = true
  }

  metadata = {
    "ssh-keys" = "${var.swarm_leader_ssh_key_user}:${file("${var.swarm_leader_ssh_key_path}")}"
  }

  labels = var.swarm_leader_labels
}

resource "yandex_compute_instance" "swarm_workers" {
  count       = var.swarm_worker_host_count
  folder_id   = var.yc_folder_id
  platform_id = var.swarm_worker_platform_id
  resources {
    cores         = var.swarm_worker_cores
    memory        = var.swarm_worker_memory
    core_fraction = var.swarm_worker_core_fraction
  }
  scheduling_policy {
    preemptible = var.swarm_worker_preemptible
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.swarm_os_image.id
      size     = var.swarm_worker_disk_size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.vm_subneta.id
    nat       = true
  }

  metadata = {
    "ssh-keys" = "${var.swarm_worker_ssh_key_user}:${file("${var.swarm_worker_ssh_key_path}")}"
  }

  labels = var.swarm_worker_labels
}
