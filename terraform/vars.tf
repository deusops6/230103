# yc iam create-token
variable "yc_iam_token" {
  type = string
}

# yc config get cloud-id
variable "yc_cloud_id" {
  type = string
}

# yc config get folder-id
variable "yc_folder_id" {
  type = string
}

variable "yc_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "net_230103_name" {
  type    = string
  default = "net_230123"
}

variable "vms_subneta_name" {
  type    = string
  default = "vmsubnet-a"
}

variable "vms_subneta_zone" {
  type    = string
  default = "ru-central1-a"
}

variable "vms_subneta_v4_cidr_blocks" {
  type    = list(string)
  default = ["192.168.10.0/24"]
}

variable "swarm_image_family" {
  type    = string
  default = "ubuntu-2204-lts"
}

variable "swarm_leader_host_count" {
  type    = number
  default = 2
}

variable "swarm_leader_platform_id" {
  type    = string
  default = "standard-v2"
}
variable "swarm_leader_preemptible" {
  type    = bool
  default = true
}
variable "swarm_leader_cores" {
  type    = number
  default = 2
}
variable "swarm_leader_memory" {
  type    = number
  default = 2
}
variable "swarm_leader_core_fraction" {
  type    = number
  default = 20
}
variable "swarm_leader_disk_size" {
  type    = number
  default = 10
}
variable "swarm_leader_ssh_key_user" {
  type    = string
  default = "ubuntu"
}
variable "swarm_leader_ssh_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}
variable "swarm_leader_labels" {
  type = map(string)
  default = {
    "ansible_group" = "swarm_leader"
  }
}

variable "swarm_worker_host_count" {
  type    = number
  default = 4
}

variable "swarm_worker_platform_id" {
  type    = string
  default = "standard-v2"
}
variable "swarm_worker_preemptible" {
  type    = bool
  default = true
}
variable "swarm_worker_cores" {
  type    = number
  default = 2
}
variable "swarm_worker_memory" {
  type    = number
  default = 2
}
variable "swarm_worker_core_fraction" {
  type    = number
  default = 20
}
variable "swarm_worker_disk_size" {
  type    = number
  default = 10
}
variable "swarm_worker_ssh_key_user" {
  type    = string
  default = "ubuntu"
}
variable "swarm_worker_ssh_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}
variable "swarm_worker_labels" {
  type = map(string)
  default = {
    "ansible_group" = "swarm_worker"
  }
}
