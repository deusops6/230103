output "nat_ip_swarm_leaders" {
  value = yandex_compute_instance.swarm_leaders[*].network_interface.0.nat_ip_address
}

output "hostname_swarm_leaders" {
  value = yandex_compute_instance.swarm_leaders[*].fqdn
}

output "nat_ip_swarm_workers" {
  value = yandex_compute_instance.swarm_workers[*].network_interface.0.nat_ip_address
}

output "hostname_swarm_workers" {
  value = yandex_compute_instance.swarm_workers[*].fqdn
}
